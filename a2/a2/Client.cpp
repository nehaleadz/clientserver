//
//  Client.cpp
//  a2
//
//  Created by neha sharma on 2014-05-20.
//  Copyright (c) 2014 neha sharma. All rights reserved.
//
//Google coding style has been used in the code

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

/********* HELPER FUNCTIONS **********************/

//Check if the string has only numbers in it
bool IsNumber(const std::string& testString)
{
    std::string::const_iterator itr = testString.begin();
    while (itr != testString.end() && std::isdigit(*itr))
    {
        itr++;
    }
    return !testString.empty() && itr == testString.end();
}

//Split input
std::vector<std::string> &split(std::string &str, char delim, std::vector<std::string> &elements) {
    std::stringstream ss(str);
    std::string element;
    while (std::getline(ss, element, delim))
    {
        elements.push_back(element);
    }
    return elements;
}

std::vector<std::string> splitInput(std::string &str, char delim)
{
    std::vector<std::string> elements;
    split(str, delim, elements);
    return elements;
}

/********* MAIN **********************/
int main(int argc, const char * argv[])
{
    if (argc < 3)
    {
		fprintf (stderr, "usage : %s <server name/ip> <server port>\n", argv[0]);
		exit (0);
	}
    
    //Create a UDP socket
    int sockfd;
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
		perror("socket");
		exit(0);
	}
    
    //Structure of the sockaddr
	struct sockaddr_in saddr;
	saddr.sin_family = AF_INET;
	saddr.sin_port = 0;
	saddr.sin_addr.s_addr = INADDR_ANY;
    
    //Bind to the socket
	if (bind(sockfd, (const struct sockaddr *) (&saddr), sizeof(struct sockaddr_in)) < 0)
    {
		perror("bind");
		exit (0);
	}
    
    // Server address
    struct sockaddr_in sa;
	unsigned short portnum;
    
	if (sscanf(argv[2], "%hu", &portnum) < 1)
    {
		fprintf(stderr, "sscanf() failed.\n");
	}
    
	// user getaddrinfo() to get server IP
	struct addrinfo *res, hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
    
	if (getaddrinfo (argv[1], NULL, (const struct addrinfo *) (&hints), &res) != 0)
    {
		perror("getaddrinfo");
		exit (0);
	}
    
	struct addrinfo *cai;
	for (cai = res; cai != NULL; cai = cai->ai_next)
    {
        if (cai->ai_family == AF_INET)
        {
			memcpy (&sa, cai->ai_addr, sizeof(struct sockaddr_in));
			break;
		}
	}
    
    sa.sin_family = AF_INET;
	sa.sin_port = htons (portnum);
    
    //Read input
    std::string input;
    std::vector<std::string>inputElements;
    
    while (std::getline(std::cin, input))
    {
        inputElements=splitInput(input, ' ');
        if (input=="STOP")
        {
            //Send stop message to the server
            std::string command="STOP";
            sendto(sockfd, command.c_str(), command.length() + 1, 0, (const struct sockaddr *)(&sa), sizeof(struct sockaddr_in));
        }
        else if (inputElements.size()>1 && IsNumber(inputElements[0]) && IsNumber(inputElements[1]))
        {
            std::string serverOutput="GET "+input;
            int len;
            if ((len = sendto(sockfd,serverOutput.c_str(), serverOutput.length() + 1, 0, (const struct sockaddr *)(&sa), sizeof(struct sockaddr_in))) < serverOutput.length() + 1)
            {
                fprintf(stderr, "Tried to send %d, sent only %d\n", (unsigned)serverOutput.length() + 1, len);
            }
            
            char buf[10000];
            memset(buf, 0, 10000);
            
            int n;
            
            while ((n= recvfrom(sockfd, buf, 10000, 0, NULL, NULL)> 0))
            {
                if (strlen(buf)==0)
                {
                    std::cerr<<"error: "<<input<<"\n";
                }
                else
                {
                    std::cout<<buf<<"\n";
                }
                break;
            }
        }
        else
        {
            //If the input does not match the above two conditions, then we print an error message
            std::cerr<<"error: invalid input\n";
        }
    }
    //Send STOP_SESSION command to the server, when the client receives EOF
    std::string command="STOP_SESSION";
    sendto(sockfd, command.c_str(), command.length() + 1, 0, (const struct sockaddr *)(&sa), sizeof(struct sockaddr_in));
    
    //Terminate client
    exit(1);
}
