//
//  Server.cpp
//  a2
//
//  Created by neha sharma on 2014-05-20.
//  Copyright (c) 2014 neha sharma. All rights reserved.
//
//Google coding style has been used in the code

#include <iostream>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <ifaddrs.h>

#include <algorithm>
#include <map>
#include <string>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <locale>


/********* HELPER FUNCTIONS **********************/

//Check if the string has only numbers in it
//Check if the string has only numbers in it
bool IsNumber(const std::string& testString)
{
    std::string::const_iterator itr = testString.begin();
    while (itr != testString.end() && std::isdigit(*itr))
    {
        itr++;
    }
    return !testString.empty() && itr == testString.end();
}

//Split input
std::vector<std::string> &split(std::string &str, char delim, std::vector<std::string> &elements) {
    std::stringstream ss(str);
    std::string element;
    while (std::getline(ss, element, delim))
    {
        elements.push_back(element);
    }
    return elements;
}

std::vector<std::string> splitInput(std::string &str, char delim)
{
    std::vector<std::string> elements;
    split(str, delim, elements);
    return elements;
}

std::string toLower(const std::string& str)
{
    std::string result;
    
    std::locale loc;
    for (int itr = 0; itr < str.length(); itr++)
    {
        result += std::tolower(str.at(itr), loc);
    }
    
    return result;
}

/********* MAIN **********************/
int main(int argc, const char * argv[])
{
    int sockfd;
    unsigned short port;
    
    if (argc < 2)
        port = 0;
	else
        port = (unsigned short)atoi(argv[1]);

    
    //Create a UDP socket
    if ((sockfd=socket(AF_INET, SOCK_DGRAM, 0))<0)
    {
        perror("Socket Initialization Error");
        exit(0);
    }
    
    //Structure of the sockaddr
    struct sockaddr_in saddr;
    saddr.sin_family=AF_INET;
    
    //Convert port in host byte order to network byte order
    saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = INADDR_ANY;
    
    //Bind to the socket
    if (bind(sockfd, (const struct sockaddr *)(&saddr),sizeof(struct sockaddr_in))<0)
    {
        perror("Binding Socket Error");
        exit(0);
    }
    
    //Set the bytes of the saddr to 0
    memset (&saddr, 0, sizeof(struct sockaddr_in));

    // In case the user didnt specify the port number, we can get it here
	int addrlen = sizeof(struct sockaddr_in);
    if (getsockname(sockfd, (struct sockaddr *) (&saddr), (socklen_t *)&addrlen) < 0)
    {
		perror ("getsockname");
		exit (0);
	}
    
    //Print the port number
    printf("%hu\n", ntohs(saddr.sin_port));
    
	memset(&saddr, 0, sizeof(struct sockaddr_in));
    
    std::string studentName,input,groupId,totalInput;
    char *studentId;
    
    //Stores name of the students with groupid-studentId being the key
    std::map<std::string,std::string>studentInfo;
    
    //Read input
    while (std::getline(std::cin,input))
    {
        std::vector<std::string>inputElements;
        
        inputElements=splitInput(input, ' ');
            
        if (inputElements.size()>1 && toLower(inputElements[0])=="group" && IsNumber(inputElements[1]))
        {
            groupId=input.substr(6,input.length());
        }
        else if (inputElements.size()>1 && IsNumber(inputElements[0]))
        {
            studentId=(char*)inputElements[0].c_str();
            studentName=input.substr(inputElements[0].length()+1,input.length());
            
            studentInfo[groupId+"-"+studentId]=studentName;
        }
        else
        {
            //If the input does not match the above two conditions, then we print an error message
            std::cerr<<"ERROR: Invalid input.Input should be of the form:'Group GroupId' or 'StudentId StudentName' \n";
        }
    }
    
    char buffer[10000];
    
	// Wait on the socket for client to send something
    int n;
    while ((n= recvfrom(sockfd, buffer, 256, 0, (struct sockaddr*) (&saddr),(socklen_t *) &addrlen)) > 0)
    {
        std::string serverClientInput=buffer;
        
        char *clientCommand;
        clientCommand=(char*)(serverClientInput.substr(0,serverClientInput.find(" "))).c_str();
        
        if (std::strncmp(clientCommand, "GET",3)==0)
        {
            std::string key=serverClientInput;
            
            key.erase(key.begin(),key.begin()+strlen(clientCommand)+1);
            replace(key.begin(),key.end(),' ','-');
            
            //Store student names in a map with key as GroupId-StudentId
            std::string serverOutput=studentInfo[key];
            
            memset(buffer, 0, 256);
            std::strcpy(buffer, serverOutput.c_str());
            
            //Send output to the client
            int len;
            if ((len = sendto(sockfd, buffer, strlen(buffer) + 1, 0, (const struct sockaddr *)(&saddr), sizeof(struct sockaddr_in))) < strlen(buffer) + 1)
            {
                fprintf(stderr, "Tried to send %d, sent only %d\n", (unsigned)strlen(buffer) + 1, len );
            }
        }
        else if (std::strncmp(clientCommand, "STOP_SESSION",strlen("STOP_SESSION"))==0)
        {
        }
        else if (std::strncmp(clientCommand, "STOP",strlen("STOP"))==0)
        {
            //Terminate the client
            break;
        }
    }
    
    close (sockfd);
    
    return 0;
}